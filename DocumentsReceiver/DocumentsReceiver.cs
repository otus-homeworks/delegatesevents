﻿using System;
using System.Collections.Generic;
using System.IO;

namespace OtusHomeWork.DocsReceiver
{
    public class DocumentsReceiver : IDisposable
    {
        private readonly List<string> _receivedDocs;

        public event Action DocumentsReady;
        public event Action TimedOut;

        private readonly FileSystemWatcher _fileSystemWatcher;
        private readonly System.Timers.Timer _timeOutWatcher;
        private bool IsDisposed;
        private bool disposedValue;

        private string _targetDir { get; set; }

        private ushort _remainingTime { get; set; }

        private const ushort MinTimeForDocsReceive = 5;

        public static List<string> MakeStandartListOfDocuments()
        {
            return new List<string>() { "Фото.jpg", "Заявление.txt", "Паспорт.jpg" };
        }

        public DocumentsReceiver(List<string> receivedDocs)
        {
            if (!(receivedDocs?.Count > 0))
            {
                Console.WriteLine("Список документов пуст, будет задан стандартный список документов");
                receivedDocs = MakeStandartListOfDocuments();
            }

            _fileSystemWatcher = new FileSystemWatcher();
            SubscribeToFileSystemWatcher(_fileSystemWatcher);

            _timeOutWatcher = new System.Timers.Timer();
            _timeOutWatcher.Interval = 1000;
            _timeOutWatcher.Elapsed += OnElapsed;

            _receivedDocs = receivedDocs;
        }

        private void SubscribeToFileSystemWatcher(FileSystemWatcher fileSystemWatcher)
        {
            fileSystemWatcher.Created += OnChangesInTargetDir;
            fileSystemWatcher.Renamed += OnChangesInTargetDir;
        }

        private void UnsubscribeAll()
        {
            _fileSystemWatcher.Created -= OnChangesInTargetDir;
            _fileSystemWatcher.Renamed -= OnChangesInTargetDir;
            _timeOutWatcher.Elapsed -= OnElapsed;
        }

        private bool IsAllTargetFilesPresent()
        {
            foreach (string fileName in _receivedDocs)
                if (!File.Exists(Path.Combine(_targetDir, fileName)))
                    return false;

            return true;
        }

        private void OnChangesInTargetDir(object sender, FileSystemEventArgs e)
        {
            if (IsAllTargetFilesPresent())
            {
                DocumentsReady?.Invoke();
                UnsubscribeAll();
            }
        }

        private void OnElapsed(Object source, System.Timers.ElapsedEventArgs e)
        {
            if (_remainingTime > 0)
                ShowRemainingTime(_remainingTime--);
            else
            {
                TimedOut?.Invoke();
                UnsubscribeAll();
            }
        }

        private void ShowRemainingTime(ushort remainingTime)
        {
            Console.CursorLeft = 0;
            Console.Write($"Осталось {remainingTime} сек. до завершения ожидания приёма документов.  ");
        }

        public void Start(string targetDirectory, ushort waitingTimeInSecs)
        {
            if (IsDisposed) throw new ObjectDisposedException("DocumentReceiver is disposed");

            if (!CheckInputParameters(targetDirectory, waitingTimeInSecs))
                return;

            _targetDir = targetDirectory;

            Console.WriteLine("Требуемые документы:");
            foreach (string fileName in _receivedDocs)
                Console.WriteLine($"  - {fileName}");

            if (IsAllTargetFilesPresent())
                Console.WriteLine("Все документы уже в наличии.");
            else
            {
                _fileSystemWatcher.Path = targetDirectory;
                _fileSystemWatcher.EnableRaisingEvents = true;

                _remainingTime = waitingTimeInSecs;
                _timeOutWatcher.Start();
            }
        }

        private bool CheckInputParameters(string targetDirectory, ushort waitingTimeInSec)
        {
            if (string.IsNullOrEmpty(targetDirectory) || !Directory.Exists(targetDirectory))
            {
                Console.WriteLine("Указан некорректный путь до директории с документами");
                return false;
            }

            if (waitingTimeInSec < MinTimeForDocsReceive)
            {
                Console.WriteLine("Задано слишком малое время ожидания приёма документов.");
                return false;
            }

            return true;
        }

        private void Dispose(bool IsDisposing)
        {
            if (IsDisposed) return;

            if (IsDisposing)
            {
                //Освобождение managed ресурсов
                UnsubscribeAll();
                _fileSystemWatcher?.Dispose();
                _timeOutWatcher?.Dispose();
            }
            //Освобождение unmanaged ресурсов
            IsDisposed = true;
        }

        public void Dispose()
        {
            // наш вызов вручную или из юзинга
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /*
        // т.к. нет неуправляемых ресурсов, не нужен
        ~DocumentsReceiver()
        {   //вызов из финализатора
            Dispose(false);
        }
        */
    }
}
