﻿using OtusHomeWork.DocsReceiver;
using System;

namespace DocumentsReceiverApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var receivedDocs = DocumentsReceiver.MakeStandartListOfDocuments();
            using var documentReceiver = new DocumentsReceiver(receivedDocs);
            documentReceiver.TimedOut += () => Console.WriteLine("\nВремя ожидания приёма документов вышло.");
            documentReceiver.DocumentsReady += () => Console.WriteLine("\nДокументы получены.");
            Console.WriteLine("Запущено ожидание приёма документов...");

            documentReceiver.Start(Environment.CurrentDirectory, 30);

            Console.ReadLine();
        }
    }
}